<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Post;

class Posts extends Controller
{

    public function show(Post $post){
        return view('post.show', ['post' => $post]);
    }

    public function search(Post $post){
        Post::with('comments')->get(); 
    }
}
