<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Project;

class Projects extends Controller
{
    public function search(Request $request) { 
        $q = $request->input('query'); 
        $projects = Project::whereRaw( "MATCH(title) AGAINST(?)", array($q) )->get(); 
        return view('projects.index', compact('projects')); 
    }
}
